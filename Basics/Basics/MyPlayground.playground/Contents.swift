import UIKit

20
23.6
"hello"

let nameConst = "red"

var color = "black"

color = nameConst

var redColor : String = "red"

print("red color value = \"\(redColor)\"")

let name = "Ivan"
let lastName = "Ivanovich"
let date = "10.10.2010"

print(name)
print(lastName)
print(date)

//HomeWork

var numberInt = 6
var numberDouble = 10.5
var numberFloat : Float = 2.5

//let sumInt = numberInt + Int(numberDouble) + Int(numberFloat) //непрвально

let sumInt = Int(Double(numberInt) + numberDouble + Double(numberFloat))

let sumDouble = Double(numberInt) + numberDouble + Double(numberFloat)

let sumFloat = Float(numberInt) + Float(numberDouble) + numberFloat


print(type(of: sumInt), type(of: sumDouble), type(of: sumFloat))

if Double(sumInt) < sumDouble {
    print("Double")
} else if Double(sumInt) == sumDouble {
    print("sumDouble equal sumInt")
} else {
    print("Int")
}


var a = (num: 15, phrase: "hello",status: true, time:2.4)

let (number, _, checkStatus, time) = a


a.time = 10
print(a)

//Optionals

var apples = "5f"

//FORCED UNWRAPPING

if Int(apples) != nil{
    print(Int(apples)!)
} else{
    print("apples contain nilvalue")
}

//OPTIONAL BINDING

if var count = Int(apples){
    count = count + 5
    print(count)
} else{
    print("apples contain nil value")
}

let age = "20"

Int(apples)


//IMPLICITLY UNWRAPPED - неявно развернутый опционал (когда объект должен быть опционалом обязательно - получаем картинку с сервера, но всегда, когда у нас к нейпоявляется доступ, мы уже можем с ней работать)

var apples2 : Int! = nil

apples2 = 4

apples2 = apples2 + 2


//Домашка по опционалам

let str1 = "10f"
let str2 = "20"
let str3 = "300"
let str4 = "40f"
let str5 = "50"

var array = [str1, str2, str3, str4, str5]
var sum = 0

for i in array{
    if let temp = Int(i){
        sum += temp
    }
}
print(sum, "apples")

var serverResponse : (statusCode : Int, message : String?, errMessage : String?) = (250, "ok", "err")


if (serverResponse.statusCode < 300 && serverResponse.statusCode > 200) {
    if let message = serverResponse.message {
        print(serverResponse.statusCode, message)
    } else {
        print(serverResponse.statusCode, "message corupted")
    }
} else {
    if let errMessage = serverResponse.errMessage {
        print(serverResponse.statusCode, errMessage)
    } else {
        print(serverResponse.statusCode, "errMessage corupted")
    }
}
    


var secondServerResponse : (message : String?, errMessage : String?) = ("err", nil)

if let message = secondServerResponse.message{
    print(message)
} else {
    print(secondServerResponse.errMessage!)
}

//print(serverResponse.errMessage)
////200 - 300
var student3 : (name: String?, car: String?, grade: Int?)

//var names : String!
